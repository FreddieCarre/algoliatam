# Query Relevancec

## Brief
> Spencer and Williams want some guidance on their optimal relevance set up. In the Algolia index that you have uploaded the data and events to, configure the relevance so that when users are searching they are seeing the results that make most sense.

## Summary
Use the Configure area of the Algolia application to have users see the most relevant results.
We have events for which items they have viewed/added to basket. We also know what they search for. Find a way to take this
information and use it to control the results being returned.

- Query suggestions?
- Searchable attributes

## Query suggestions
This will help a user refine their search whilst typing.
By suggesting queries and categories, they will be able to be inspired for a more specific search term.
Also, it could be enhanced to display some products as they type. This will help them to
refine as they go and create a more relevant search.

# Searchable attributes
Set searchable attributes:

- name -> users can ask for a specific product
- description -> wide net to capture details about a product
- brand -> users can ask for a specific brand
- type -> get results by type of product
- categories -> find generic categories of product

The other attributes didn't seem very useful from a search perspective e.g. price and image.

# Relevency Optimisations
These would require some more planning and experimentation to see how users search. A question which might be investigated through this would be "do users benefit more from lenient type
