function transformData(productList) {
  return productList
    .map((product) => {
      const price = product.price;

      // Product has no price. Unable to complete transformation.
      // Log name for now, but should find a way to handle these cases properly.
      if (price === null || typeof price === 'undefined') {
        console.log(`Product has no price ${product.name}`);
        return;
      }

      return {
        ...product,
        price: Math.floor(product.price * 0.8)
      };
    })
    .filter(p => p !== undefined);
};

module.exports = {
  transformData
}

