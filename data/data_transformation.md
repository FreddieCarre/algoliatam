# Data Transformation

## Brief
> Spencer and Williams are having a sale on Cameras. They would like you create and run a demo script that reduces the price of everything in the camera category by 20% and then round to the lowest full number. They have provided the raw data as products.json . The data should be transformed and sent to algolia in a single script.

## Summary

I chose to do this in JavaScript, I would normally opt for TypeScript but felt I didn't need the typings for a task of this size.
JavaScript makes serializing JSON data nice and straight-forward.

### Transformation
I started by writing the transformer logic `productTransforms.js`.
I added some tests for this to check I had covered the required business logic.

It's a relatively simple map over the product list. I added a check for the price being present so that we can log the ID and skip that product, this would then allow for manual intervention. This may not be necessary depending on the use case, but I think for eCommerce it would be appropriate.

### Reading products
Reading the data from the `products.json` file using fs, and parsing the string.
As a basic data access layer, this could be swapped out for reading from a database.

### Uploading to Algolia
Heavily inspired by the Algolia documentation and examples in the repo.
Set App ID, API Key and Index name from .env file to manage secrets.
Index name was decided to be `spencer_and_williams_products` as it is descriptive of what the data is, although the `spencer_and_williams` prefix may be unnecessary given the index would possibly exist within the Spencer and Williams account.

We connect to the index using `initIndex` which creates the index if it does not already exist.
When saving the objects, autoGenerateObjectIDIfNotExist is explicitly set to false as the products have an objectID field.
On successfully saving the objects, we log a message to say how many have been saved.
On error, we log the message.
