const fs = require('fs');

function getAllProducts() {
  const fileContent = fs.readFileSync('./products.json', 'utf-8');

  return JSON.parse(fileContent);
}

module.exports = {
  getAllProducts
}
