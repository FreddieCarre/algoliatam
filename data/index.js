const { getIndex, setRelevance, uploadObjects } = require('./algoliaClient');
const { getAllProducts } = require('./dataAccess');
const { transformData } = require('./productTransforms');

const products = getAllProducts();
const transformedProducts = transformData(products);

const index = getIndex();
setRelevance(index);
uploadObjects(index, transformedProducts);
