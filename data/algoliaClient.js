const dotenv = require('dotenv');
const algolia = require('algoliasearch');

dotenv.config();

const ALGOLIA_APP_ID = process.env.ALGOLIA_APP_ID;
const ALGOLIA_API_KEY = process.env.ALGOLIA_API_KEY;
const ALGOLIA_INDEX_NAME = process.env.ALGOLIA_INDEX_NAME;

function getIndex() {
  const client = algolia(ALGOLIA_APP_ID, ALGOLIA_API_KEY);
  return client.initIndex(ALGOLIA_INDEX_NAME);
}

function setRelevance(index) {
  index
    .setSettings({
      searchableAttributes: [
        'name',
        'description',
        'brand',
        'type',
        'categories',
      ],
    })
    .then(() => {
      console.log('Settings set successfully.');
    });
}

function uploadObjects(index, objects) {
  index
    .saveObjects(objects, { autoGenerateObjectIDIfNotExist: false })
    .then(({ objectIDs }) => {
      console.log(`Successfully saved ${objectIDs.length} objects`);
    })
    .catch((err) => {
      console.log(`Failed saving objects ${err.message}`);
    });
}

module.exports = {
  getIndex,
  setRelevance,
  uploadObjects,
};
