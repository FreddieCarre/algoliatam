# Algolia Insights

## Brief
> Our customer Spencer and Williams want to use Personalisation in order to to do this they need to implement **_Algolia Insights_** . They have asked for us to create a demo of the events included in their provided codebase.
>
> It is imperative that we send clicks and conversion on the result page hit results, any other events included will be a bonus.

## Summary

### Research
Started with the Algolia Academy (awesome tool by the way).

Then used the documentation and code snippets to get started.

Added the script to `index.html` to fetch the insights source code.

Setup the insights middleware in `results-page.js` to forward events and set the user token.
To improve on this, we would need a means of getting an identifier for the user from their session.

I then installed the Insights Validator Chrome extension and opened the Events debugger from the Algolia dashboard.

`results-page.js` required some changes:
- Add the `bindEvent` parameter to the `resultHit` function
- Call the `bindEvent` parameter within the View and Add to Cart buttons, with a relevant event name

These changes got the events coming into the debugger.
Although the buttons are not clickable, the click events can be triggered from the browser console.

Other events which could be tracked:
- Completed checkout (remember to pass queryId through to components for the checkout journey)
