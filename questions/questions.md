# Questions

## Question 1

> From: marissa@startup.com
> Subject:  Bad design
>
> Hello,
>
> Sorry to give you the kind of feedback that I know you do not want to hear, but I really hate the new dashboard design. Clearing and deleting indexes are now several clicks away. I am needing to use these features while iterating, so this is inconvenient.
>
> Thanks,
> Marissa

### Notes
- Take it on board, perhaps some tooling could be created which uses the Algolia SDK to clear/delete indexes.
- https://www.algolia.com/doc/guides/sending-and-managing-data/manage-your-indices/how-to/delete-multiple-indices/

### Response
Hello Marissa,

I'm sorry to hear that you've been having problems with using our dashboard.

I will pass your feedback on to our design team so that they can keep it in mind when looking at potential changes to our user experience.

In the meantime, I believe you would benefit from using our API directly to quickly create, clear and delete indices.

We would be more than happy to help your development team design what this would look like, but at a high level we would look at creating some tools to automate these tasks. Your development team may want to take a look at our documentation to get a feel for what's possible, some key areas are linked below.

Firstly, indices can be cleared using the `index.clearObjects()` function, which is described [clear objects function](https://www.algolia.com/doc/api-reference/api-methods/clear-objects/).

For deleting indices, we would use the `index.delete()` function as documented [delete function](https://www.algolia.com/doc/api-reference/api-methods/clear-objects/)

If that sounds like something that you would be interested in, let me know and I shall organise a plannign session. Also, should you have any further questions, do not hesitate to contact me.

Kind regards,
Freddie

---

## Question 2

> From: carrie@coffee.com
> Subject: URGENT ISSUE WITH PRODUCTION!!!!
>
> Since today 9:15am we have been seeing a lot of errors on our website. Multiple users have reported that they were unable to publish their feedbacks and that an alert box with "Record is too big, please contact enterprise@algolia.com".
>
> Our website is an imdb like website where users can post reviews of coffee shops online. Along with that we enrich every record with a lot of metadata that is not for search. I am already a paying customer of your service, what else do you need to make your search work?
>
> Please advise on how to fix this. Thanks.

### Notes
- Records are too large (Algolia has a limit of X Mb)
  - Remove metadata as doesn't need to be indexed

### Response
Dear Carrie,

I'm sorry to hear about your production issue. Fortunately, we have some options for dealing with this issue.

From what you've told me, it sounds like the metadata which the records are enriched with has caused the records to be too large. We limit the size of records for performance reasons, the limits are explained in more detail here [record limits](https://support.algolia.com/hc/en-us/articles/4406981897617-Is-there-a-size-limit-for-my-index-records-/). For your plan, the limit is {insert size here}.

With that in mind, I would propose that we remove the metadata from the records.

As the metadata is not used for searching, you would benefit from not including it in the record.
It is best to only provide information which is needed for searching, displaying and ranking.

Please get in touch if you have any questions on the above, or if you would like to schedule a call to get into more detail.

Kind regards,
Freddie

---

## Question 3

> From: marc@hotmail.com
> Subject: Error on website
>
> Hi, my website is not working and here's the error:
>
> ![error message](./error.png)
>
> Can you fix it please?

### Notes
- Looks like the searchkit is not imported.
  - Check the node modules/ CDN script in html

### Response
Hello Marc,

I'm sorry to hear your website isn't working and I would be more than happy to help.
I will provide some information about the error you have shared, but to help further, I would need some more information. Would you be free to join a call and our team would be able to help you to debug this issue?

From the stack trace, it looks like you are trying to use `searchkit`. This is not an Algolia product, it is a library for creating a React / GraphQL integration with Elasticsearch.
If you are trying to integrate with the Algolia search API, you would need to use `InstantSearch` the installation of which is documented on the [Building Search UI](https://www.algolia.com/doc/guides/building-search-ui/getting-started/react/) section of our documentation.

Let me know if you would like to arrange that call and we can help you get setup.

Kind regards,
Freddie
